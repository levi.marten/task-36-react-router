import React, { useState } from 'react';

const RegisterForm = props => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [registerError, setRegisterError] = useState('');
    const onUsernameChanged = ev => setUsername(ev.target.value.trim());
    const onPasswordChanged = ev => setPassword(ev.target.value.trim());

    const onRegisterClicked = async ev => {
        setIsLoading(true);

        const API_REGISTER_URL = 'https://survey-poodle.herokuapp.com/v1/api/users/register';
        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                user: {
                    username,
                    password
                }
            })
        };
        let result;

        try {
            await fetch(API_REGISTER_URL, fetchOptions).then(resp => resp.json())
            .then(resp => {
                if(resp.status >= 400) {
                    throw Error(resp.error) 
                } else {
                    setRegisterError('')
                    result = true;
                };
            });
        } catch (e) {
            setRegisterError(e.message || e);
        } finally {
            setIsLoading(false);
            props.complete(result);
        }
    };

    return (
        <form>
            <div>
                <label hidden>Username </label>
                <input type="text" placeholder="Choose a username..." onChange={onUsernameChanged} >
                </input>
            </div>
            <div>
                <label hidden>Password </label>
                <input type="password" placeholder="Choose a password..." onChange={onPasswordChanged}  >
                </input>
            </div>
            <div>
                <button type="button" onClick={onRegisterClicked}>Sign up</button>
            </div>
            {isLoading && <div><br />Registering user...</div>}
            {registerError && <div><br />Error: {registerError}</div>}
        </form>
    )
};

export default RegisterForm;