import React from 'react';
import '../App.css';
import RegisterForm from '../Forms/RegisterForm'
import { Link, useHistory } from 'react-router-dom'

function Register() {

  const history = useHistory();
  const handleRegisterComplete = (result) => {
    if (result) {
      history.replace('/dashboard');
    };
  };

  return (
    <div className="Register">
      <h1>Welcome!</h1>
      <h2>Register to SurveyPuppy</h2>
      <RegisterForm complete={handleRegisterComplete} />
      < br />
      <Link to="/login">Already have an account? Login here!</Link>
    </div>
  );
};

export default Register;
