import React from 'react';
import '../status404.css';
import { Link } from 'react-router-dom'


function status404() {
    return (
        <div className="status404">
           <img src="https://www.elegantthemes.com/blog/wp-content/uploads/2020/02/000-404.png" alt="404 - Page not found" />
            <br />
            <Link to="/login">Go back to the start page!</Link>
        </div>
    );
};

export default status404;