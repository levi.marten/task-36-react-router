import React from 'react';
import '../App.css';
import LoginForm from '../Forms/LoginForm'
import { Link } from 'react-router-dom'

function Login() {
    return (
        <div className="Login">
            <h1>Welcome!</h1>
            <h2>Login to SurveyPuppy</h2>
            < LoginForm />
            < br />
            <Link to="/register">Don't have an account yet? Register here!</Link>
        </div>
    );
};

export default Login;
