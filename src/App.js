import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import status404 from './Containers/status404';
import Register from './Containers/Register';
import Login from './Containers/Login';
import Dashboard from './Containers/Dashboard';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={ Login } />
          <Route path="/login" component={ Login } />
          <Route path="/home" component={ Login } />
          <Route path="/register" component={ Register } />
          <Route path="/dashboard" component={ Dashboard } />
          <Route path="*" component={ status404 } />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
