import React from 'react';

const DashboardMessage = () => {

    return (
        <div>
            <h1>Welcome to the SurveyPuppy dashboard!</h1>
            <h2>How can we help you today?</h2>
        </div>
    );
};

export default DashboardMessage;